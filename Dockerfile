# This Dockerfile is used for a youtube tutorial
# base image - nginx with tag "latest"
FROM nginx:latest


# Adding custom index.html hosted on Github Gist
#ADD https://bitbucket.org/jquinther/web_prueba/raw/028caf43c562686b7daf6d81fcd7fd8545f45707/index.html /usr/share/nginx/html/
#ADD https://bitbucket.org/jquinther/web_prueba/raw/028caf43c562686b7daf6d81fcd7fd8545f45707/elements.html /usr/share/nginx/html/
#ADD https://bitbucket.org/jquinther/web_prueba/raw/028caf43c562686b7daf6d81fcd7fd8545f45707/left-sidebar.html /usr/share/nginx/html/
#ADD https://bitbucket.org/jquinther/web_prueba/raw/028caf43c562686b7daf6d81fcd7fd8545f45707/right-sidebar.html /usr/share/nginx/html/

ADD . /usr/share/nginx/html/



# Adding read permissions to custom index.html
#RUN chmod +r /usr/share/nginx/html/index.html
#RUN chmod +r /usr/share/nginx/html/elements.html
#RUN chmod +r /usr/share/nginx/html/left-sidebar.html
#RUN chmod +r /usr/share/nginx/html/right-sidebar.html

# 'nginx -g daemon off" will run as default command when any container is run that uses the image that was built using this Dockerfile"
CMD ["nginx", "-g", "daemon off;"]